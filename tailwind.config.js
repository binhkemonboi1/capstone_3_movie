/** @type {import('tailwindcss').Config} */
module.exports = {
  important: true,
  // khi có important thì tailwind sẽ đè hết antd
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
