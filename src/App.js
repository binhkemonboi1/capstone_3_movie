import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import QuanLy from "./Component/admintemplate/QuanLy";
import Film from "./Component/admintemplate/Film";
import User from "./Component/admintemplate/User";
import ShowTime from "./Component/admintemplate/ShowTime";
import DangNhap from "./Component/HomePage/DangNhap";
import HomePage from "./Component/HomePage/HomePage";
import DetailPage from "./Component/DetailPage/DetailPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/detail/:id" element={<DetailPage />} />
        <Route path="/dangnhap" element={<DangNhap />} />
        <Route path="/quanly" element={<QuanLy />}>
          <Route path="/quanly/user" element={<User />} />
          <Route path="/quanly/film" element={<Film />} />
          <Route path="/quanly/showtime" element={<ShowTime />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
