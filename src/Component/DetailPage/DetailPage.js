import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { movieService } from '../../service/service';
import { Progress, Rate } from 'antd';
import TabMovie from '../HomePage/TabMovie/TabMovie';

export default function DetailPage() {
    let { id } = useParams();
    const [detail, setDetail] = useState();
    useEffect(() => {
        movieService
            .getDetail(id)
            .then((res) => {
                setDetail(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="container ">
            <div className="flex items-center">
                <img className="w-1/3" src={detail?.hinhAnh} alt="" />
                <Progress
                    strokeWidth={20} //kích cỡ circle
                    size={200}
                    strokeColor={"red"} //màu circle
                    format={(number) => {
                        return (
                            <p className="animate-spin text-blue-600 text-xl font-extrabold"> {number / 10} Điểm  </p>
                        );
                    }}
                    type="circle" percent={detail?.danhGia * 10} />
                <Rate allowHalf value={detail?.danhGia}
                    count={10}
                    className="text-red-500" />
                {/* tính sao */}
            </div>
            <div><TabMovie /></div>
        </div>
    );
}
