import axios from "axios";
import { store } from "..";
// import { turnOffLoadingAction, turnOnLoadingAction } from "../redux/action/spinner";

const TOKEN_CYBERSOFT =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1NiIsIkhldEhhblN0cmluZyI6IjE4LzA0LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxMzM5ODQwMDAwMCIsIm5iZiI6MTY4MzMwNjAwMCwiZXhwIjoxNzEzNTQ2MDAwfQ.4A7jJib1YUkmnIr-QDcjs_3j1YY0Ft1wPZDfe8qFrqE";

export let https = axios.create({
    baseURL: "https://movienew.cybersoft.edu.vn",
    headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
    },
});
// let dispatch = useDispatch();
// dispatch ngoai component


// axios interceptor
// Add a request interceptor
// https.interceptors.request.use(function (config) {
//     // Do something before request is sent
//     console.log("api di");
//     store.dispatch(turnOnLoadingAction());
//     return config;
// }, function (error) {
//     // Do something with request error
//     return Promise.reject(error);
// });

// // Add a response interceptor
// https.interceptors.response.use(function (response) {
//     console.log("api ve");
//     store.dispatch(turnOffLoadingAction());
//     // Any status code that lie within the range of 2xx cause this function to trigger
//     // Do something with response data
//     return response;
// }, function (error) {
//     // Any status codes that falls outside the range of 2xx cause this function to trigger
//     // Do something with response error
//     return Promise.reject(error);
// });